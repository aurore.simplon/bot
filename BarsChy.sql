--sqlite

CREATE TABLE Etablissements (
    Id INTEGER PRIMARY KEY,
    Nom TEXT NOT NULL, 
    Adresse TEXT, 
    CP INTEGER, 
    Ville TEXT, 
    Description TEXT
);

CREATE TABLE Photos (
    Id INTEGER PRIMARY KEY,
    page TEXT,
    url TEXT,
    IdEtablissement INTEGER NOT NULL,
    FOREIGN KEY(IdEtablissement) REFERENCES Etablissements(Id)
);